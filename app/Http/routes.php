<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// Authentication Routes...

Route::group(['middleware' => ['web']], function () {

    Route::auth();

    // Route::get('/', 'TaskController@index');
    // Route::post('/task', 'TaskController@store');
    // Route::delete('/task/{task}', 'TaskController@destroy');

    Route::group(['middleware' => ['role:SuperAdmin']], function () {
        Route::get('/', 'JobController@index');
        Route::get('/job', 'JobController@index');
        Route::post('/job', 'JobController@store');
        Route::delete('/job/{job}', 'JobController@destroy');
    });
});
