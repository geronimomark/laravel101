<?php

namespace App\Contracts;

interface RepositoryInterface
{
    public function all();
 
    // public function paginate();
 
    public function create(array $data);
 
    public function update(array $data, $id);
 
    public function delete(array $data);
 
    public function find(array $data);
 
    // public function findBy();
}
