<!-- resources/views/jobs.blade.php -->

@extends('layouts.app', ['title' => $title])

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')

        <!-- New Job Form -->
        <form action="{{ url('job') }}" method="POST" class="form-horizontal">
            {!! csrf_field() !!}

            <!-- Job Name -->
            <div class="form-group">
                <label for="job" class="col-sm-3 control-label">Jobs</label>

                <div class="col-sm-6">
                    <input type="text" name="title" id="job-title" class="form-control">
                    <input type="text" name="description" id="job-description" class="form-control">
                </div>
            </div>

            <!-- Add Job Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Add Job
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!-- Current Tasks -->
    @if (count($jobs) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Jobs
            </div>

            <div class="panel-body">
                <table class="table table-striped job-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>Job</th>
                        <th>&nbsp;</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($jobs as $job)
                            <tr>
                                <!-- Job Name -->
                                <td class="table-text">
                                    <div>{{ $job->title }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $job->description }}</div>
                                </td>

                                <!-- Delete Button -->
                                <td>
                                    <form action="{{ url('job/'.$job->id) }}" method="POST">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}

                                        <button type="submit" class="btn btn-danger">
                                            <i class="fa fa-trash"></i> Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif


@endsection