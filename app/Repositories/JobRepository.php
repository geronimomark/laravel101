<?php

namespace App\Repositories;

use App\Repositories\AbstractRepository;
use App\Models\Job;

class JobRepository extends AbstractRepository
{
    public function __construct(Job $job)
    {
        $this->model = $job;
    }
}
