<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\JobRepository as Job;

class JobController extends Controller
{
    private $job;

    public function __construct(Request $request, Job $job)
    {
        $this->middleware('auth');

        $this->job = $job;
    }

    public function index(Request $request)
    {
        return view('jobs.index', [
            'jobs' => $this->job->all(),
            'title' => 'MarkG'
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         => 'required|max:255',
            'description' => 'required|max:255',
        ]);

        $this->job->create($request->except('_token'));

        return redirect('/job');
    }

    public function destroy(Request $request, $id)
    {
        $this->job->delete([
            'id' => $id,
        ]);

        return redirect('/job');
    }
}