<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Task'  => 'App\Policies\TaskPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('SuperAdmin-Access', function($user){
            return $user->role == 'SuperAdmin';
        });

        $gate->define('Admin-Access', function($user){
            return $user->role == 'Admin';
        });

        $gate->define('Ops-Access', function($user){
            return $user->role == 'Ops';
        });

        $gate->define('Plumber-Access', function($user){
            return $user->role == 'Plumber';
        });
        
        //
    }
}
