<?php

namespace App\Http\Middleware;

use Gate;
use Closure;

class Role
{
    public function handle($request, Closure $next, $role)
    {
        if (Gate::denies($role . '-Access', $request->user())) {
            return redirect('/logout');
        }
            
        return $next($request);
    }
}
