<?php

namespace App\Repositories;

use App\Contracts\RepositoryInterface;

abstract class AbstractRepository implements RepositoryInterface
{
    protected $model;

    public function all()
    {
        return $this->model->get();
    }
 
    public function create(array $data)
    {
        return $this->model->create($data);
    }
 
    public function update(array $data, $id)
    {
        return $this->model->where('id', '=', $id)->update($data);
    }
 
    public function delete(array $data)
    {
        return $this->model->destroy($data);
    }
 
    public function find(array $data)
    {
        return $this->model->where($data)->get();
    }
}
