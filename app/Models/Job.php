<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description',
    ];

    /**
     * Get all of the tasks for the user.
     */
    // public function tasks()
    // {
    //     return $this->hasMany(Task::class);
    // }
}
