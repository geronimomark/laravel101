<?php

namespace App\Http\Middleware;

use Gate;
use Closure;

class AdminRole
{
    public function handle($request, Closure $next, $role)
    {
        if (Gate::denies($role . '-Access', $request->user())) {
            return response('blank', 404);
        }
            
        return $next($request);
    }
}
